<?php
//ejemplo 1 array unidimencional
$a=array("rojo","verde","azul","amarillo");

echo"<br/>";
for ($i=0; $i <4 ; $i++) { 
    echo "[".$i."]".$a[$i]."<br/>";
}
echo"Ejemplo1";
echo"<br/>";
echo "El array tiene ";
//sintaxis para usar la Funcion count en un arreglo unidimensional
//Nota importante no obligatorio usar el parametro 0, la funcion incluso funcionara para contar los elementos del arreglo unidimensional
echo count($a,0). "elementos";

echo"<br/>";
echo"Ejemplo 2";
echo"<br/>";
//En caso del que el arreglo se contenga a si mismo LA FUNCION COUNT contara  un numero mayor de lo que es realmente.
//como en este caso
$b=array(
   array
   ("rojo"),
   array
   ("marron")
);

echo "La matriz tiene ";
//sintaxis para usar la Funcion count en un arreglo multidimensional usando el parametro 1
echo count($b,1)." elementos";

?>