<?php
//Esta funcion array_keys_exist sirve para saber si existe o no un identificador o llave dentro de un arreglo unidimencional,
//lo que hace esta funcion es devolver un verdadero o 1 si el identificador existe en el arreglo de lo contrario no devuelve un
//falso o 0

$a=array("nombre"=>"grover","edad"=>"28");

if(array_key_exists("hj",$a))
{
    echo"El identificador existe";
}
else{
    echo"El identificador no existe";
}



//otra forma de usar el array

$b=array_key_exists("nombre",$a);
if($b==true)
{
    echo"El identificador existe";
}
else{
    echo"El identificador no existe";
}

?>